<?php

require_once 'operaciones.php';
require_once 'pi.php';
require_once 'about.php';
require_once 'home.php';

define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

if (!empty($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = 'home'; 
}

if(isset($action)){
    $partesURL = explode("/", $action);
    $operacion = $partesURL[0];

    switch($operacion) {
        case 'home':
            getHome();
            break;        
        case 'sumar':
            echo suma($partesURL[1], $partesURL[2]);
            break;
        case 'restar':
            echo resta($partesURL[1], $partesURL[2]);
            break;
        case 'multiplicar':
            echo multiplicacion($partesURL[1], $partesURL[2]);
            break;
        case 'dividir':
            echo division($partesURL[1], $partesURL[2]);
            break;
        case 'pi':
            echo getPi();
            break;
        case 'about':
            if(isset($partesURL[1])){
                echo getAbout($partesURL[1]);
            }
            else {
                echo getAbout(null);
            }
            break;
        default:
            echo "Error! Operación no permitida.";
            break;
    }
}

?>

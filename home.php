<?php

function getHome() {  
    $html = '
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <base href="' . BASE_URL .'" target="_blank">
        <link rel="stylesheet" href="css/style.css">
        <title>Calculadora | About</title>
    </head>
    <body>

        <nav id="main-nav">
            <ul>
                <li><a href="home" target="_self">Calculadora</a></li>
                <li><a href="pi" target="_self">Número Pi</a></li>
                <li><a href="about" target="_self">About</a></li>
            </ul>
        </nav>

        <h1>Calculadora</h1>
        <form id="form-calc" method="GET">         
          <label>Valor A:<input type="numeric" name="operandoX"  required/></label>
          <label>Valor B:<input type="numeric" name="operandoY" required/></label>
          <select required name="operacion">
            <option value="sumar">Sumar</option>
            <option value="restar">Restar</option>
            <option value="dividir">Dividir</option>
            <option value="multiplicar">Multiplicar</option>
          </select>
  
          <input type="submit" value="Calcular!"> 
        </form>
  
        <section id="resultado">
          
        </section>
        
        <script src="js/main.js"></script>
      </body>
      </html>
    ';
  
    echo $html;
  }


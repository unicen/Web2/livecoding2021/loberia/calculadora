<?php

function getPi() {

    $html = '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <link rel="stylesheet" href="css/style.css">
            <title>Calculadora | Pi</title>
        </head>
        <body>
            <nav id="main-nav">
                <ul>
                    <li><a href="home" target="_self">Calculadora</a></li>
                    <li><a href="pi" target="_self">El número Pi</a></li>
                    <li><a href="about" target="_self">Sobre Nosotros</a></li>
                </ul>
            </nav>';

    $html .= "<h1>El número Pi</h1>";
    $html .= "<p>π (pi) es la relación entre la longitud de una circunferencia y su diámetro en geometría euclidiana.​ Es un número irracional y una de las constantes matemáticas más importantes. Se emplea frecuentemente en matemáticas, física e ingeniería.</p>";
    $html .= "<p>El valor es: <strong>" . pi() . "<strong></p>";

    $html .= '</body>
    </html>';

    return $html;

}

?>

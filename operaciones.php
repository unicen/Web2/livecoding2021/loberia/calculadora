<?php

	function suma($op1, $op2) {
		$resultado = $op1 + $op2;
		return $resultado;
	}

	function resta($op1, $op2) {
		$resultado = $op1 - $op2;
		return $resultado;
	}

	function multiplicacion($op1, $op2) {
		$resultado = $op1 * $op2;
		return $resultado;
	}

	function division($op1, $op2) {
		if ($op2 == 0) {
			echo 'Error: El operador 2 es cero.';
			die();
		}
		$resultado = $op1 / $op2;
		return $resultado;
	}

?>
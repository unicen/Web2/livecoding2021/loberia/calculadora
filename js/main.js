"use strict"

document.querySelector('#form-calc').addEventListener('submit', enviar)

const resultadoContainer = document.querySelector('#resultado');

async function enviar(e) {
    e.preventDefault(); 
    const method = this.method;

    const formData = new FormData(this); // FormData representa los campos de un formulario y sus valores
    const url =  `${formData.get('operacion')}/${formData.get('operandoX')}/${formData.get('operandoY')}`;
    
    let response = await fetch(url, {
        method: method
    });
    let html = await response.text();

    resultadoContainer.innerHTML = 'Resultado: ' + html;
}
